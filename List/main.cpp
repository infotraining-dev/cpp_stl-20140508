#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

template <typename Container>
void print(const Container& c, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = c.begin();
        it != c.end(); ++it )
        cout << *it << " ";
    cout << "]\n";
}

int main()
{
    list<int> lst_int1 = { 1, 2, 3, 4, 5 };

    print(lst_int1, "lst_int1");

    list<int>::iterator it1 = lst_int1.begin();
    advance(it1, 3);

    cout << "*it = " << *it1 << endl;

    auto it2 = lst_int1.end();

    cout << "No of items from it1 to i2: " << distance(it1, it2) << endl;

    int numbers[] = { 6, 0, 8, 3, 5, 9, 2, 3, 2, 3, 2, 3, 2 };

    list<int> lst_int2(begin(numbers), end(numbers));

    print(lst_int2, "lst_int2");

    lst_int2.sort();

    print(lst_int2, "lst_int2");

    lst_int1.merge(lst_int2);

    print(lst_int1, "lst_int1 po merge");
    print(lst_int2, "lst_int2 po merge");

    lst_int1.remove(3);

    print(lst_int1, "lst_int1");

    it1 = lst_int1.begin();
    it2 = lst_int1.end();

    advance(it1, 3);
    advance(it2, -3);

    lst_int2.splice(lst_int2.begin(), lst_int1, it1, it2);

    print(lst_int1, "lst_int1 po splice");
    print(lst_int2, "lst_int2 po splice");

    list<int>::iterator where = find(lst_int1.begin(), lst_int1.end(), 6);

    if (where != lst_int1.end())
    {
        cout << "znalazlem 6 i usuwam." << endl;
        it1 = lst_int1.erase(where);
        if (it1 != lst_int1.end())
            cout << "Nastepny element usunietej 6: " << *it1 << endl;
    }
    else
        cout << "Nie znalazlem 6." << endl;

    print(lst_int1, "lst_int1");

    for(auto rit = lst_int1.rbegin(); rit != lst_int1.rend(); ++rit)
        cout << *rit << " ";
    cout << endl;
}
