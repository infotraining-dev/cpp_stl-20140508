#include <iostream>
#include <vector>
#include <iterator>
#include <stdexcept>
#include <memory>
#include <cassert>

using namespace std;

template <typename Container>
void print(const Container& c, const string& prefix)
{
//    // C++11
//    cout << prefix << ": [ ";
//    for(const auto& item : c)
//        cout << item << " ";
//    cout << "]\n";

    // C++03
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = c.begin();
        it != c.end(); ++it )
        cout << *it << " ";
    cout << "]\n";
}

//class Iterator
//{
//    Node* node_;
//public:
//    Iterator& operator++()  // ++it
//    {
//        node_ = node_->next;
//        return *this;
//    }

//    Iterator operator++(int) // it++
//    {
//        Iterator tmp(*this);
//        node_ = node_->next;
//        return tmp;
//    }
//};

class X
{
public:
    X()
    {
        cout << "dc X()" << endl;
    }

    X(const X& x)
    {
        cout << "cc X()" << endl;
    }

    X(X&& x)
    {
        cout << "mvc X()" << endl;
    }

    X& operator=(const X&)
    {
        cout << "= X()" << endl;
    }

    X&& operator=(X&& x)
    {
        cout << "mv= X()" << endl;
    }

    ~X()
    {
        cout << "~X()" << endl;
    }
};

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(" << p.id() << ", " << p.name() << ")";
    return out;
}

int main()
{
    vector<int> vec_int1(10);

    cout << "vec_int1.size: " << vec_int1.size() << endl;
    cout << "vec_int1.capacity: " << vec_int1.capacity() << endl;

    print(vec_int1, "vec_int1");

    vector<int> vec_int2(15, -1);

    print(vec_int2, "vec_int2");

    vector<int> vec_int3 = { 1, 2, 3, 4, 5, 6 };

    print(vec_int3, "vec_int3");

    vector<int> vec_int4(10, 33) ;

    print(vec_int4, "vec_int4");

    int numbers[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    vector<int> vec_int5(begin(numbers), end(numbers));

    print(vec_int5, "vec_int5");

    vector<int> vec_int6(vec_int5.begin(), vec_int5.begin() + 5);

    print(vec_int6, "vec_int6");

    int& first = vec_int6.front();
    int& last = vec_int6.back();

    cout << "first: " << first << " " << "last: " << last << endl;

    try
    {
        cout << "thrd item: " << vec_int6.at(6) << endl;
    }
    catch(const out_of_range& e)
    {
        cout << "Exception: " << e.what() << endl;
    }

    cout << "max_size vector<int>: " << vec_int6.max_size() << endl;

    vector<string> words1 = { "one", "two", "three", "four" };

    cout << "max_size vector<string>: " << words1.max_size() << endl;

    // vector<string> words2(words1.max_size() - 100); // throws bad_alloc

    // iteratory - C++03
    for(vector<string>::const_iterator it = words1.begin();
        it != words1.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    // iteratory - C++11
    for(auto it = words1.cbegin(); it != words1.cend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    // iteratory wsteczne - C++11
    for(auto it = words1.crbegin(); it != words1.crend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    cout << "words1 size: " << words1.size()
         << " capacity: " << words1.capacity() << endl;

    words1.push_back(string("five"));
    words1.insert(words1.end(), "six");
    words1.emplace_back("five");

    print(words1, "words1");

    cout << "words1 size: " << words1.size()
         << " capacity: " << words1.capacity() << endl;


    vector<Person> people
            = { Person{1, "Kowalski"}, Person{2, "Nowak"} };

    Person nowacki { 7, "Nowacki"};
    people.push_back(std::move(nowacki));
    cout << "After move: " << nowacki << endl;
    people.push_back(Person{3, "Nijaki"});
    people.emplace_back(4, "Iks");

    for(auto& p : people)
        cout << p << "\n";

    unique_ptr<Person> up1(new Person{6, "Iksinski"});
    unique_ptr<Person> up2 = std::move(up1);

    assert(up1.get() == nullptr);

    vector<unique_ptr<Person>> people_ptr;
    people_ptr.push_back(std::move(up2));
    people_ptr.push_back(unique_ptr<Person>(new Person{9, "Ygrek"}));
    people_ptr.emplace_back(new Person{10, "Zet"});

    cout << "\nPeople_ptr:\n";
    for(auto& ptr : people_ptr)
    {
        cout << *ptr << "\n";
    }
    cout << endl;

    // Efektywne wykorzystanie wektora
    vector<int> vec_numbers;
    vec_numbers.reserve(256);

    for(int i = 0; i < 256; ++i)
    {
        vec_numbers.push_back(i);
    }
    cout << "capacity: " << vec_numbers.capacity() << "\n";
    vec_numbers.resize(10);
    //vec_numbers.shrink_to_fit();
    // zmniejszenie rozmiaru bufora w C++03
    vector<int>(vec_numbers).swap(vec_numbers);

    cout << "capacity: " << vec_numbers.capacity() << "\n";

    {
        vector<Person*> vec_raw_ptrs;

        vec_raw_ptrs.push_back(new Person {9, "Em"});

        delete vec_raw_ptrs[0];
        vec_raw_ptrs[0] = new Person {10, "Er"};

        for(auto ptr : vec_raw_ptrs)
            delete ptr;

        vec_raw_ptrs.clear();
    }

    cout << "\n\n";

    vector<X> vec_x(4);
    cout << "After construction..." << endl;

    vec_x[2] = X();

    cout << "end of main..." << endl;
}
