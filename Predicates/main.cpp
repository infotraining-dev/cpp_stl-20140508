#include <iostream>
#include <vector>
#include <functional>
#include <boost/algorithm/string.hpp>
#include <boost/checked_delete.hpp>

using namespace std;

template <typename InIt, typename Func1>
void mfor_each(InIt start, InIt end, Func1 f)
{
    while(start != end)
    {
        f(*start);
        ++start;
    }
}

void print(int item)
{
    cout << "Print: " << item << endl;
}

void print_with_prefix(int item, const string& prefix)
{
    cout << prefix << item << endl;
}


class Printer : public std::unary_function<int, void>
{
    int row_index_ = 0;
public:
    void operator()(int x)
    {
        cout << "Printing " << ++row_index_ << ". item: "
             << x << "\n";
    }
};

class Lambda_2364782354273547235 : public std::unary_function<int, void>
{
    int& value_;
public:
    Lambda_2364782354273547235(int& value) : value_(value)
    {}
    void operator()(int item)
    { cout << item * value_ << " "; }
};

class CompareStringsCaseInsensitive
        : public std::binary_function<string, string, bool>
{
public:
    bool operator()(const string& str1, const string& str2) const
    {
        return boost::to_lower_copy(str1) < boost::to_lower_copy(str2);
    }
};

class IsEven : public unary_function<int, bool>
{
public:
    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void print() const
    {
        cout << "Person(" << id_ << ", " << name_ << ")\n";
    }

    void print_with_prefix(const string& prefix) const
    {
        cout << prefix << ": ";
        print();
    }
};

//// PIMPL

//// server.hpp

//class Server
//{
//    class Impl;
//    unique_ptr<Impl> pimpl_;
//public:
//    Server();
//    ~Server();
//    void send(const string& message);
//};

//// server.cpp

//class Server::Impl
//{
//    vector<char> buffer_;
//    //...
//};

//Server::Server() : pimpl_(new Server::Impl)
//{
//}

//Server::~Server() = default;


int main()
{
    vector<int> vec_int = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

// C++03
//    vector<int>::iterator where = find_if(vec_int.begin(), vec_int.end(),
//                                          bind2nd(greater<int>(), 5));

    vector<int>::iterator where
            = find_if(vec_int.begin(), vec_int.end(),
                      bind(greater<int>(), placeholders::_1, 5));

    if (where != vec_int.end())
        cout << "Znalazlem element > 5: " << *where << endl;

    copy_if(vec_int.begin(), vec_int.end(),
            ostream_iterator<int>(cout, " "), not1(IsEven()));
    cout << "\n";

    mfor_each(vec_int.begin(), vec_int.end(), &print);

    cout << "\n";
    mfor_each(vec_int.begin(), vec_int.end(), Printer());

    cout << "\n";
    const int value = 5;
    const char sep = ' ';
    mfor_each(vec_int.begin(), vec_int.end(),
              [=, &value](int item) { cout << item * value << sep; });
    cout << "\n";

    cout << "value: " << value << endl;

    mfor_each(vec_int.begin(), vec_int.end(),
              [](int x) { print_with_prefix(x, "Item: "); });

    vector<Person> people1 = { Person{1, "Kowalski"}, Person{2, "Nowak"},
                               Person{3, "Adamski"} };

    // for_each(people1.begin(), people1.end(), mem_fun_ref(&Person::print)); // C++03
    for_each(people1.begin(), people1.end(), mem_fn(&Person::print));

    cout << "\n\n";

    for_each(people1.begin(), people1.end(),
             bind(&Person::print_with_prefix, placeholders::_1, "Person"));

    cout << "\n\n";

    vector<Person*> people2 = { new Person{1, "Kowalski"}, new Person{2, "Nowak"},
                               new Person{3, "Adamski"} };

    //for_each(people2.begin(), people2.end(), mem_fun(&Person::print)); // C++03
    for_each(people2.begin(), people2.end(), mem_fn(&Person::print));
    //for_each(people2.begin(),  people2.end(), [](Person* p) { p->print(); });

    for_each(people2.begin(), people2.end(), &boost::checked_delete<Person>);

    vector<int> vec(100);
    vec.push_back(67);

    generate_n(vec.begin(), 100, bind(modulus<int>(), bind(&rand), 100));

    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << "\n\n";

    auto garbage_start = remove(vec.begin(), vec.end(), 67);
    vec.erase(garbage_start, vec.end());

    sort(vec.begin(), vec.end());
    garbage_start = unique(vec.begin(), vec.end());
    vec.erase(garbage_start, vec.end());

    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << "\n\n";
}
