#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>

using namespace std;

int main()
{
    vector<int> vec = { 1, 2, 3, 14, 5, 6, 17, 8, 9, 10 };

    stack<int, vector<int>> stack_int;
    queue<int> queue_int;
    priority_queue<int, vector<int>, greater<int>> proqueue_int;

    for(auto item : vec)
    {
        stack_int.push(item);
        queue_int.push(item);
        proqueue_int.push(item);
    }

    cout << "Stack: ";
    while (!stack_int.empty())
    {
        cout << stack_int.top() << " ";
        stack_int.pop();
    }

// Niebezpieczne wykonanie top() i pop() na pustym stosie
//    int temp = stack_int.top();
//    cout << " Temp: " << temp << endl;
//    stack_int.pop();

    cout << "\n\nQueue: ";
    while(!queue_int.empty())
    {
        cout << queue_int.front() << " ";
        queue_int.pop();
    }

    cout << "\n\nPriorityQueue: ";
    while(!proqueue_int.empty())
    {
        cout << proqueue_int.top() << " ";
        proqueue_int.pop();
    }

    // Bitset
    cout << "\n\n";
    bitset<16> bts1("0101010");
    cout << "bts1: " << bts1 << endl;
    bts1.flip(0);
    bts1.flip(bts1.size()-1);
    bts1.flip();
    cout << "bts1: " << bts1 << endl;

    unsigned long value = bts1.to_ulong();
    cout << "value: " << value << endl;

    bitset<16> bts2(26757);
    cout << "bts2: " << bts2 << endl;

    cout << "~bts1 & ((bts2<<1) ^ bts1): "
         << (~bts1 & ((bts2<<1) ^ bts1)) << endl;

    if (bts2.test(0))
        cout << "najmlodszy bit w bts2 jest ustawiony\n";
}
