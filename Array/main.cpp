#include <iostream>
#include <array>

using namespace std;

template <typename Container>
void print(const Container& c, const string& prefix)
{
    // C++03
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = c.begin();
        it != c.end(); ++it )
        cout << *it << " ";
    cout << "]\n";
}

void foo(int tab[], size_t size)
{

}

int main()
{
    array<int, 10> tab1 = { 1, 2, 3, 4, 5 };

    cout << "tab1.size = " << tab1.size() << endl;

    tab1.at(9) = 9;

    print(tab1, "tab1");

    //tab1.fill(-1);

    foo(tab1.data(), tab1.size());

    print(tab1, "tab1");

    array<int, 10> tab2 = tab1;

    print(tab2, "tab2");
}
