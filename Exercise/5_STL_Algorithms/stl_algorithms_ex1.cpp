#include "utils.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

int main()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec, "vec: ");

    using namespace placeholders;

    // 1a - wyświetl parzyste
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            //[](int x) { return x % 2 == 0;});
            bind(not2(modulus<int>()), _1, 2));
    cout << "\n";

    // 1b - wyswietl ile jest nieparzystych
    cout << "Nieparzystych: ";
    cout << count_if(vec.begin(), vec.end(), [](int x) { return x % 2;}) << endl;

    // 1c - wyswietl ile jest parzystych
    cout << "Parzystych: ";
    cout << count_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0;}) << endl;

    // 2 - usuń liczby podzielne przez 3
    vec.erase(remove_if(vec.begin(), vec.end(),
              //[](int x) { return x % 3 == 0;}),
              bind(not2(modulus<int>()), _1, 3)),
              vec.end());

    // 3 - tranformacja: podnieś liczby do kwadratu
    //transform(vec.begin(), vec.end(), vec.begin(), vec.begin(), multiplies<int>());
    //transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x;});

    float (*fun)(float, float) = &pow;
    transform(vec.begin(), vec.end(), vec.begin(),
              bind(fun, _1, 2));
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";


    // 4 - wypisz 5 najwiekszych liczb
    cout << "najwieksze: ";
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    copy(vec.begin(), vec.begin() + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    // 5 - policz wartosc srednia
    double sum = 0.0;
    for_each(vec.begin(), vec.end(), [&](int x) { sum += x;});
    cout << "avg: " << sum/vec.size() << endl;

    cout << "avg: " <<
            accumulate(vec.begin(), vec.end(), 0.0)
                / vec.size() << endl;

    // 6 - wyswietl wszystkie mniejsze od 50
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
    //        [](int x) { return x < 50;});
            bind(less<int>(), _1, 50));
    cout << "\n";
}
