#include "utils.hpp"

#include <vector>
#include <random>
#include <set>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

class RandGen
{
    int range_;
    random_device* rd;
    mt19937 *gen;
public:
    RandGen(int range) : range_(range)
    {
        rd = new random_device;
        gen = new mt19937((*rd)());
    }

    int operator()() const
    {
        uniform_int_distribution<> dis(0, range_);
        return dis(*gen);
    }
};

int main()
{
    vector<int> vec(25);
    generate(vec.begin(), vec.end(), RandGen(30));

    vector<int> sorted_vec(vec);
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset(vec.begin(), vec.end());

    print(vec, "vec: ");
    print(sorted_vec, "sorted_vec: ");
    print(mset, "mset: ");

    // czy 17 znajduje sie w sekwencji?

    // ile razy wystepuje 22?

    // usun wszystkie wieksze od 15
}
