#include <iostream>
#include <fstream>
#include <unordered_set>
#include <string>
#include <chrono>
#include <boost/algorithm/string.hpp>

using namespace std;

void print_params(const unordered_set<string>& uo)
{
    cout << "Bucket count: " << uo.bucket_count()
         << "; Load factor: " << uo.load_factor()
         << endl;
}

int main()
{
     // wszytaj zawartość pliku en.dict ("słownik języka angielskieog")
     // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;
    string input_text = "this is an exmple of snetence";
    boost::split(words_list, input_text, boost::is_any_of("\t "));

    unordered_set<string> dictionary;
    cout << "max_load_factor: " << dictionary.max_load_factor() << endl;
    dictionary.max_load_factor(3.0);

    print_params(dictionary);

    // wczytanie slownika
    ifstream fin("../en.dict");
    string word;

    size_t count_before = 23;

    while (fin >> word)
    {
        dictionary.insert(word);
        if (dictionary.bucket_count() != count_before)
        {
            print_params(dictionary);
            count_before = dictionary.bucket_count();
        }
    }

    print_params(dictionary);

    // sprawdzenie pisowni
    auto start = chrono::high_resolution_clock::now();

    for (auto& word : words_list)
    {
        if (dictionary.count(word) == 0)
        {
            cout << "Bad word: " << word << endl;
        }
    }

    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed time: "
         << chrono::duration_cast<chrono::nanoseconds>(end-start).count()
         << " ns" << endl;
}

