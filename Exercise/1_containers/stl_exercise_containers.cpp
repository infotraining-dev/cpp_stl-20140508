#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>
#include <deque>

using namespace std;

void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    vector<int> vec_int(begin(tab), end(tab));

    // 2 - wypisz wartość średnią przechowyanych liczb w vec_int
    avg(vec_int.data(), vec_int.size()); // C++11
    avg(&vec_int[0], vec_int.size()); // C++03

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    auto vec_cloned = vec_int;
    //decltype(vec_int) vec_cloned2;

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    vec_int.clear();
    vec_int.shrink_to_fit();

    int rest[] = { 1, 2, 3, 4 };
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    vec_cloned.insert(vec_cloned.end(), begin(rest), end(rest));

    // 5 - posortuj zawartość wektora vec_cloned
    sort(vec_cloned.begin(), vec_cloned.end(), greater<int>());

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    cout << "\nvec_cloned: ";
    for(auto it = vec_cloned.cbegin(); it != vec_cloned.cend(); ++it)
        cout << *it << " ";
    cout << "\n\n";

    // 6a
    deque<int> dq_int(vec_cloned.begin(), vec_cloned.end());

    dq_int.insert(dq_int.begin(), begin(rest), end(rest));

    cout << "\ndq_int: ";
    for(auto it = dq_int.cbegin(); it != dq_int.cend(); ++it)
        cout << *it << " ";
    cout << "\n\n";

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned
    // TODO

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze
    // TODO

    // 9 - usuń liczby parzyste z kontenera
    // TODO

    // 10 - wyświetl elementy kontenera w odwrotnej kolejności
    cout << "numbers: ";
    // TODO
    cout << "\n\n";

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    // TODO

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    cout << "lst_numbers: ";
    // TODO
    cout << "\n\n";
}

