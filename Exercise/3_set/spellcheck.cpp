#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <chrono>
#include <boost/algorithm/string.hpp>

using namespace std;

int main()
{
     // wszytaj zawartość pliku en.dict ("słownik języka angielskieog")
     // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;
    string input_text = "this is an exmple of snetence";
    boost::split(words_list, input_text, boost::is_any_of("\t "));

    // wczytanie slownika
    ifstream fin("../en.dict");

    istream_iterator<string> start_file(fin);
    istream_iterator<string> end_file;

    set<string> dictionary(start_file, end_file);

    // sprawdzenie pisowni
    auto start = chrono::high_resolution_clock::now();

    for (auto& word : words_list)
    {
        if (dictionary.count(word) == 0)
        {
            cout << "Bad word: " << word << endl;
        }
    }

    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed time: "
         << chrono::duration_cast<chrono::nanoseconds>(end-start).count()
         << " ns" << endl;
}

