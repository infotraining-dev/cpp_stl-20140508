#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <functional>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <unordered_map>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien
    danego slowa w pliku tekstowym.
    Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

typedef vector<string> WordsContainer;

WordsContainer load_words_from_file(const string& file_name)
{
    ifstream file_book(file_name);

    if (!file_book)
    {
        cout << "Blad otwarcia pliku..." << endl;
        exit(1);
    }

    string line;
    WordsContainer words;

    while (getline(file_book, line))
    {
        boost::tokenizer<> tok(line);
        words.insert(words.end(), tok.begin(), tok.end());
    }

    return words;
}

int main()
{
    WordsContainer words = load_words_from_file("../holmes.txt");

    cout << "no of words: " << words.size() << endl;

    typedef unordered_map<string, int> Concordance;
    //typedef map<string, int> Concordance;

    Concordance concordance(128000);

    // sprawdzenie pisowni
    auto start = chrono::high_resolution_clock::now();

    // zliczenie wystapien
    for(auto& word : words)
        ++concordance[boost::to_lower_copy(word)];
        //++concordance[word];

    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed time: "
         << chrono::duration_cast<chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    std::cout << "Bucket count: " << concordance.bucket_count()
              << " Load factor: " << concordance.load_factor() << endl;

    // wyswietlenie 10 najczesciej wystepujacych slow
    typedef multimap<int, string, greater<int>> Result;
    Result result;

    for(Concordance::iterator it = concordance.begin();
        it != concordance.end(); ++it)
    {
        result.insert(make_pair(it->second, it->first));
    }

    int count = 0;
    for(Result::iterator it = result.begin();
        it != result.end(); ++it, ++count)
    {
        if (count == 10)
            break;

        cout << it->second << " - " << it->first << endl;
    }
}
