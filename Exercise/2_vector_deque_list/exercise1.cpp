#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>

using namespace std;

/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

template <typename Container, typename Func>
void fill_container(Container& c, Func f, size_t n)
{
    for(size_t i = 0; i < n; ++i)
        c.push_back(f());
}

// szablony funkcji umożliwiające sortowanie kontenera
template <typename Container>
void sort(Container& c)
{
    std::sort(begin(c), end(c));
}

template <typename T>
void sort(std::list<T>& lst)
{
    lst.sort();
}

clock_t get_clock()
{
    clock_t ct = clock();

    if (ct == clock_t(-1))
    {
        cerr << "Clock error" << endl;
        exit(1);
    }

    return ct;
}

typedef int Data;

namespace Vector
{
    typedef std::vector<Data> Container;
}

namespace Deque
{
    typedef std::vector<Data> Container;
}

namespace List
{
    typedef std::list<Data> Container;
}

int main()
{
    namespace Impl = List;

    const int n = 100000000;


    clock_t t1, t2;

    // utwórz kontener
    Impl::Container container;

    cout << "Preparing data... Filling container." << endl;

    t1 = get_clock();

    // wypełnij kontener danymi
    fill_container(container, rand, n);

    t2 = get_clock();

    cout << "Time: " << double(t2-t1)/CLOCKS_PER_SEC << "sec." << endl;

    cout << "\nStart sorting..." << endl;

    t1 = get_clock();

    // posortuj kontener
    sort(container);

    t2 = get_clock();

    cout << "Time: " << double(t2-t1)/CLOCKS_PER_SEC << "sec." << endl;
}
