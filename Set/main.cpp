#include <iostream>
#include <set>
#include <map>
#include <unordered_set>
#include <boost/functional/hash.hpp>

using namespace std;

class Person
{
    int id_;
    string name_;
public:
    Person(int id = 0, const string& name = "") : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator<(const Person& p) const
    {
        return id_ < p.id_;
    }

    bool operator ==(const Person& p) const
    {
        return p.id() == id() && p.name() == name();
    }
};

class PersonHash
{
public:
    size_t operator()(const Person& p) const
    {
//        hash<int> hasher;
//        return hasher(p.id());

        size_t seed = 0;
        boost::hash_combine(seed, p.id());
        boost::hash_combine(seed, p.name());

        return seed;
    }
};

class PersonByNameComparer
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        return p1.name() < p2.name();
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(" << p.id() << ", " << p.name() << ")";
    return out;
}

template <typename Container>
void print(const Container& c, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = c.begin();
        it != c.end(); ++it )
        cout << *it << " ";
    cout << "]\n";
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "(" << p.first << ", " << p.second << ")";

    return out;
}

int main()
{
    set<int, greater<int>> numbers;

    numbers.insert(4);
    numbers.insert(9);

    int arr[] = { 1, 5, 4, 2, 6, 7, 8 , 10 };
    numbers.insert(begin(arr), end(arr));

    pair<set<int, greater<int>>::iterator, bool> result = numbers.insert(8);

    if (result.second)
        cout << "Wstawilem " << *result.first << endl;
    else
        cout << *result.first << " jest juz w zbiorze" << endl;

    print(numbers, "numbers");

    auto where = numbers.find(6);


    if (where != numbers.end())
        cout << "Znalazlem " << *where << endl;
    else
        cout << "Nie znalazlem elementu." << endl;

    multiset<int> mset(numbers.begin(), numbers.end());

    for(int i = 0; i < 5; ++i)
        mset.insert(6);

    print(mset, "mset");

    cout << "Ilosc 6: " << mset.count(6) << endl;

    for(auto it = mset.lower_bound(6);  it != mset.upper_bound(6); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    set<Person, PersonByNameComparer> people;

    people.insert(Person{1, "Kowalski"});
    people.insert(Person{4, "Nowak"});
    people.insert(Person{2, "Anonim"});

    print(people, "people");

    // map
    typedef map<int, Person> Team;

    Team team = { {9, Person(9, "Nijaki")},
                  {7, Person(7, "Siedem")} };

    team.insert(Team::value_type(1, Person{1, "Kowalski"}));
    team.insert(pair<int, Person>(3, Person{3, "Nowak"}));
    team.insert(make_pair(2, Person {2, "Anonim"}));
    team[4] = Person{4, "Zet"};

    auto it = team.find(5);
    if (it != team.end())
        cout << *it << endl;
    else
        cout << "Brak pary o kluczu 5\n";

    print(team, "team");

    unordered_set<Person, PersonHash> hashed_people;

    hashed_people.insert(Person {1, "Kowalski"});
    hashed_people.insert(Person {2, "Nowak"});
    hashed_people.insert(Person {4, "Nijaki"});

    cout << "Count Nijaki: " << hashed_people.count(Person{4, "Nijki"}) << endl;
}
