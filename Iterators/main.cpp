#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <deque>
#include <iterator>

using namespace std;

template <typename InIt, typename OutIt>
OutIt mcopy(InIt first, InIt last, OutIt dest)
{
    while (first != last)
    {
        *dest++ = *first++;
    }

    return dest;
}

bool is_even(int x)
{
    return x % 2 == 0;
}


template <typename Container>
void print(const Container& c, const string& prefix)
{
    // C++03
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = c.begin();
        it != c.end(); ++it )
        cout << *it << " ";
    cout << "]\n";
}

int main()
{
    vector<int> vec_int = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    list<int> lst_int(vec_int.size());

    list<int>::iterator it =  copy(vec_int.begin(),
                                   vec_int.begin() + 5, lst_int.begin());

    copy(vec_int.begin() + 5, vec_int.end(), it);

    for(auto item : lst_int)
        cout << item << " ";
    cout << endl;

    deque<int>  dq_evens;
    front_insert_iterator<deque<int>> front_insert_it(dq_evens);

    *front_insert_it = 13;  // dq_evens.push_front(13)

    remove_copy_if(vec_int.begin(), vec_int.end(),
            back_inserter(dq_evens), &is_even);

    print(dq_evens, "dq_evens");

    copy(vec_int.begin(), vec_int.end(), inserter(dq_evens, dq_evens.begin() + 3));

    print(dq_evens, "dq_evens");

    // iteratory strumienia we-wy
    istream_iterator<int> start(cin);
    istream_iterator<int> end;

    vector<int> data(start, end);

    cout << "data: ";
    copy(data.begin(), data.end(),
         ostream_iterator<int>(cout, " "));
    cout << endl;
}
